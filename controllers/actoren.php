<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class actoren_controller extends process_controller {
		private function show_overview($case_id) {
			if (($actors = $this->model->get_actors($case_id)) === false) {
				$this->output->add_tag("result", "Database error.");
				return;
			}

			$actor_chance = config_array(ACTOR_CHANCE);
			$actor_knowledge = config_array(ACTOR_KNOWLEDGE);
			$actor_resources = config_array(ACTOR_RESOURCES);

			$this->output->open_tag("overview");

			$this->output->open_tag("actors");
			foreach ($actors as $actor) {
				$actor["chance"] = $actor_chance[$actor["chance"] - 1];
				$actor["knowledge"] = $actor_knowledge[$actor["knowledge"] - 1];
				$actor["resources"] = $actor_resources[$actor["resources"] - 1];
				$this->output->record($actor, "actor");
			}
			$this->output->close_tag();

			$this->output->close_tag();
		}

		private function show_actor_form($actor) {
			$this->output->open_tag("edit");

			$pulldowns = array(
				"chance"    => ACTOR_CHANCE,
				"knowledge" => ACTOR_KNOWLEDGE,
				"resources" => ACTOR_RESOURCES);

			foreach ($pulldowns as $name => $options) {
				$options = config_array($options);
				$this->output->open_tag($name);
				foreach ($options as $option) {
					$this->output->add_tag("item", $option);
				}
				$this->output->close_tag();
			}

			$this->output->record($actor, "actor");

			$this->output->close_tag();
		}

		private function export_overview($case_id) {
			if (($bia_items = $this->model->get_bia_items($case_id)) === false) {
				$this->output->add_tag("result", "Database error.", array("url" => "bia/".$case_id));
				return;
			}

			if (($actors = $this->model->get_actors($case_id)) === false) {
				$this->output->add_tag("result", "Database error.");
				return;
			}

			$pdf = new PDF_report($this->case["title"]);
			$pdf->SetTitle($this->case["name"]);
			$pdf->SetAuthor($this->user->fullname);
			$pdf->SetSubject("Overzicht informatiesystemen");
			$pdf->SetKeywords("RAVIB, overzicht, business impact analyse");
			$pdf->SetCreator("RAVIB - https://www.ravib.nl/");
			$pdf->AliasNbPages();

			$pdf->AddPage();
			$pdf->AddChapter("Overzicht informatiesystemen");
			$pdf->Ln(8);

			/* Informationsystems
			 */
			$pdf->SetFont("helvetica", "B", 11);
			$pdf->Cell(75, 6, "Informatiesysteem", "B");
			$pdf->Cell(20, 6, "Beschik.", "B");
			$pdf->Cell(20, 6, "Integri.", "B");
			$pdf->Cell(25, 6, "Vertrouw.", "B");
			$pdf->Cell(20, 6, "Waarde", "B");
			$pdf->Cell(10, 6, "SE", "B");
			$pdf->Cell(10, 6, "Loc", "B");
			$pdf->Ln(8);

			foreach ($bia_items as $nr => $item) {
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(75, 5, ($nr + 1).": ".$item["item"]);
				$pdf->SetFont("helvetica", "", 10);
				$pdf->Cell(20, 5, $this->model->availability_score[$item["availability"] - 1]);
				$pdf->Cell(20, 5, $this->model->integrity_score[$item["integrity"] - 1]);
				$pdf->Cell(25, 5, $this->model->confidentiality_score[$item["confidentiality"] - 1]);
				$pdf->Cell(20, 5, $this->model->asset_value($item));
				$pdf->Cell(10, 5, is_true($item["owner"]) ? "ja" : "nee");
				$pdf->Cell(10, 5, $item["location"]);
				$pdf->Ln(5);

				$pdf->AddTextBlock("Omschrijving", $item["description"]);
				$pdf->AddTextBlock("Impact van incident", $item["impact"]);
				$pdf->Ln(1);
			}

			$pdf->AddPage();

			/* Actors
			 */
			$pdf->AddChapter("Actoren");
			$pdf->SetFont("helvetica", "B", 11);
			$pdf->Cell(85, 6, "Naam", "B");
			$pdf->Cell(40, 6, "Kans / bereidheid", "B");
			$pdf->Cell(30, 6, "Kennisniveau", "B");
			$pdf->Cell(25, 6, "Middelen", "B");
			$pdf->Ln(8);

			$actor_chance = config_array(ACTOR_CHANCE);
            $actor_knowledge = config_array(ACTOR_KNOWLEDGE);
            $actor_resources = config_array(ACTOR_RESOURCES);

			$pdf->SetFont("helvetica", "", 10);
			foreach ($actors as $actor) {
				$pdf->Cell(85, 5, $actor["name"]);
				$pdf->Cell(40, 5, $actor_chance[$actor["chance"] - 1]);
				$pdf->Cell(30, 5, $actor_knowledge[$actor["knowledge"] - 1]);
				$pdf->Cell(25, 5, $actor_resources[$actor["resources"] - 1]);
				$pdf->Ln(5);
				if (trim($actor["reason"]) != "") {
					$pdf->Cell(5, 5, "");
					$pdf->Cell(13, 5, "Reden: ");
					$pdf->Cell(162, 5, $actor["reason"]);
					$pdf->Ln(5);
				}
			}
			$pdf->Ln(10);

			/* Chance
			 */
			$pdf->AddChapter("Inschatten van de kans");
			$pdf->Write(5, "Keuzemogelijkheden: ");
			$pdf->MultiCell(140, 5, implode(", ", $this->model->risk_matrix_chance));
			$pdf->Ln(10);

			/* Impact values
			 */
			$pdf->AddChapter("Invulling van de impact");
			$impact_values = json_decode($this->case["impact"], true);
			foreach ($this->model->risk_matrix_impact as $i => $impact) {
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(35, 5, $impact.": ");
				$pdf->SetFont("helvetica", "", 10);
				$pdf->Write(5, $impact_values[$i]);
				$pdf->Ln(5);
			}
			$pdf->Ln(10);

			/* Action
			 */
			$pdf->AddChapter("Betekenis van de aanpak");
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Beheersen:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->Write(5, "Het nemen van maatregelen om de impact van en de kans op een incident te verkleinen.");
			$pdf->Ln(5);
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Ontwijken:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->Write(5, "Het nemen van maatregelen om de kans op een incident te verkleinen.");
			$pdf->Ln(5);
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Verweren:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->Write(5, "Het nemen van maatregelen om de impact van een incident te verkleinen.");
			$pdf->Ln(5);
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Accepteren:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->Write(5, "Het accepteren van de gevolgen indien een incident zich voordoet.");
			$pdf->Ln(15);

			/* Scales
			 */
			$pdf->AddChapter("Schalen bij BIA en Actoren");
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Beschikbaarheid:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(AVAILABILITY_SCORE)));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Integriteit:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(INTEGRITY_SCORE)));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Vertrouwelijkheid:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(CONFIDENTIALITY_SCORE)));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Waarde:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(ASSET_VALUE_LABELS)));

			$pdf->Ln(3);

			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Kans / bereidheid:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(ACTOR_CHANCE)));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Kennisniveau:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(ACTOR_KNOWLEDGE)));

			/* Output
			 */
			$this->output->disable();
			$case_name = $this->generate_filename($this->case["name"]);
			$pdf->Output("BIA ".$case_name.".pdf", "I");
		}

		public function execute() {
			$case_id = $this->page->pathinfo[1];
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			$this->show_breadcrumbs($case_id);

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Actor opslaan") {
					/* Save actor
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_actor_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create actor
						 */
						if ($this->model->create_actor($_POST, $case_id) === false) {
							$this->output->add_message("Fout bij aanmaken van actor.");
							$this->show_actor_form($_POST);
						} else {
							$this->user->log_action("actor created");
							$this->show_overview($case_id);
						}
					} else {
						/* Update actor
						 */
						if ($this->model->update_actor($_POST, $case_id) === false) {
							$this->output->add_message("Fout bij wijzigen van actor.");
							$this->show_actor_form($_POST);
						} else {
							$this->user->log_action("actor updated");
							$this->show_overview($case_id);
						}
					}
				} else if ($_POST["submit_button"] == "Actor verwijderen") {
					/* Delete actor
					 */
					if ($this->model->delete_actor($_POST["id"], $case_id) === false) {
						$this->output->add_message("Fout bij verwijderen van actor.");
						$this->show_actor_form($_POST);
					} else {
						$this->user->log_action("actor deleted");
						$this->show_overview($case_id);
					}
				} else {
					$this->show_overview($case_id);
				}
			} else if ($this->page->pathinfo[2] === "export") {
				/* Export overview
				 */
				$this->export_overview($this->page->pathinfo[1]);
			} else if ($this->page->pathinfo[2] === "new") {
				/* New actor
				 */
				$actor = array();
				$this->show_actor_form($actor);
			} else if (valid_input($this->page->pathinfo[2], VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit actor
				 */
				if (($actor = $this->model->get_actor($this->page->pathinfo[2], $case_id)) === false) {
					$this->output->add_tag("result", "actor not found.\n");
				} else {
					$this->show_actor_form($actor);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview($case_id);
			}
		}
	}
?>
