<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class bia_controller extends process_controller {
		private function show_overview($case_id) {
			if (($items = $this->model->get_items($case_id)) === false) {
				$this->output->add_tag("result", "Database error.", array("url" => "bia/".$case_id));
				return;
			}

			$this->output->open_tag("overview", array("case_id" => $case_id));
			foreach ($items as $item) {
				$item["value"] = $this->model->asset_value($item);
				$item["availability"] = $this->model->availability_score[$item["availability"] - 1];
				$item["confidentiality"] = $this->model->confidentiality_score[$item["confidentiality"] - 1];
				$item["integrity"] = $this->model->integrity_score[$item["integrity"] - 1];
				$item["owner"] = is_true($item["owner"]) ? "ja" : "nee";
				$this->output->record($item, "item");
			}
			$this->output->close_tag();
		}

		private function show_bia_form($item) {
			$this->output->open_tag("edit", array("case_id" => $item["case_id"]));

			$this->output->open_tag("availability");
			foreach ($this->model->availability_score as $value => $label) {
				$this->output->add_tag("label", $label, array("value" => $value + 1));
			}
			$this->output->close_tag();

			$this->output->open_tag("confidentiality");
			foreach ($this->model->confidentiality_score as $value => $label) {
				$this->output->add_tag("label", $label, array("value" => $value + 1));
			}
			$this->output->close_tag();

			$this->output->open_tag("integrity");
			foreach ($this->model->integrity_score as $value => $label) {
				$this->output->add_tag("label", $label, array("value" => $value + 1));
			}
			$this->output->close_tag();

			$locations = config_array(BIA_LOCATIONS);
			$this->output->open_tag("location");
			foreach ($locations as $value => $label) {
				$this->output->add_tag("label", $label, array("value" => $value));
			}
			$this->output->close_tag();

			$item["owner"] = show_boolean($item["owner"]);
			$this->output->record($item, "item");

			$this->output->close_tag();
		}

		public function execute() {
			$case_id = $this->page->pathinfo[1];
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			$this->show_breadcrumbs($case_id);

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Informatiesysteem opslaan") {
					/* Save item
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_bia_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create item
						 */
						if ($this->model->create_item($_POST, $case_id) === false) {
							$this->output->add_message("Fout bij aanmaken informatiesysteem.");
							$this->show_bia_form($_POST);
						} else {
							$this->user->log_action("bia created");
							$this->show_overview($case_id);
						}
					} else {
						/* Update item 
						 */
						if ($this->model->update_item($_POST, $case_id) === false) {
							$this->output->add_message("Fout tijdens bijwerken informatiesysteem.");
							$this->show_bia_form($_POST);
						} else {
							$this->user->log_action("bia updated");
							$this->show_overview($case_id);
						}
					}
				} else if ($_POST["submit_button"] == "Informatiesysteem verwijderen") {
					/* Delete item 
					 */
					if ($this->model->delete_item($_POST["id"], $case_id) == false) {
						$this->output->add_message("Error deleting item.");
						$this->show_bia_form($_POST);
					} else {
						$this->user->log_action("bia deleted");
						$this->show_overview($case_id);
					}
				} else {
					$this->show_overview($case_id);
				}
			} else if ($this->page->pathinfo[2] === "new") {
				/* New item
				 */
				$item = array("case_id" => $case_id);
				$this->show_bia_form($item);
			} else if (valid_input($this->page->pathinfo[2], VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				/* Edit item
				 */
				if (($item = $this->model->get_item($this->page->pathinfo[2], $case_id)) === false) {
					$this->output->add_tag("result", "Item not found.\n", array("url" => "bia/".$case_id));
				} else {
					$this->show_bia_form($item);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview($case_id);
			}
		}
	}
?>
