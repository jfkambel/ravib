<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class actoren_model extends process_model {
		private $columns = array();

		public function get_actors($case_id) {
			$query = "select * from actors where case_id=%d order by name";

			return $this->db->execute($query, $case_id);
		}

		public function get_actor($actor_id, $case_id) {
			$query = "select * from actors where id=%d and case_id=%d limit 1";
			if (($result = $this->db->execute($query, $actor_id, $case_id)) == false) {
				return false;
			}

			return $result[0];
		}

		public function save_oke($actor) {
			$result = true;

			if (trim($actor["name"]) == "") {
				$this->output->add_message("Geef de actor een naam.");
				$result = false;
			}

			$pulldowns = array(
                "chance"    => ACTOR_CHANCE,
                "knowledge" => ACTOR_KNOWLEDGE,
                "resources" => ACTOR_RESOURCES);

			foreach ($pulldowns as $variable => $options) {
				$max = count(config_array($options));
				if (($actor[$variable] < 1) || ($actor[$variable] > $max)) {
					$this->output->add_message("Ongeldige optie gekozen.");
					$result = false;
				}
			}

			return $result;
		}

		public function create_actor($actor, $case_id) {
			$keys = array("id", "case_id", "name", "chance", "knowledge", "resources", "reason");

			$actor["id"] = null;
			$actor["case_id"] = $case_id;

			return $this->db->insert("actors", $actor, $keys);
		}

		public function update_actor($actor, $case_id) {
			if ($this->get_actor($actor["id"], $case_id) == false) {
				return false;
			}

			$keys = array("name", "chance", "knowledge", "resources", "reason");

			return $this->db->update("actors", $actor["id"], $actor, $keys);
		}

		public function delete_actor($actor_id, $case_id) {
			if ($this->get_actor($actor["id"], $case_id) == false) {
				$result = false;
			}

			return $this->db->delete("actors", $actor_id);
		}

		public function get_bia_items($case_id) {
			return $this->borrow("bia")->get_items($case_id);
		}

		public function asset_value($item) {
			return $this->borrow("bia")->asset_value($item);
		}
	}
?>
