<?php
	/* Copyright (C) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * http://www.banshee-php.org/
	 */

	class banshee_page_model extends model {
		public function get_page($url) {
			$query = "select * from pages where url=%s and language=%s";

			if (($result = $this->db->execute($query, $url, $this->output->language)) == false) {
				return false;
			}

			return $result[0];
		}
	}
?>
