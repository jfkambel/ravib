function set_focus() {
	username = document.getElementById("username");
	password = document.getElementById("password");
	
	if (username.value == "") {
		username.focus();
	} else {
		password.focus();
	}
}

function use_demo_account() {
	document.getElementById("username").value = "demo";
	document.getElementById("password").value = "demo";

	document.getElementById("loginform").submit();
}
