-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: ravib_release
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actors`
--

DROP TABLE IF EXISTS `actors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_id` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `chance` tinyint(3) unsigned NOT NULL,
  `knowledge` tinyint(3) unsigned NOT NULL,
  `resources` tinyint(3) unsigned NOT NULL,
  `reason` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`case_id`),
  CONSTRAINT `actors_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actors`
--
-- ORDER BY:  `id`

LOCK TABLES `actors` WRITE;
/*!40000 ALTER TABLE `actors` DISABLE KEYS */;
INSERT INTO `actors` VALUES (1,1,'Cybercriminelen',2,3,3,'Persoonsgegevens vanuit de enquêtes.'),(2,1,'Statelijke actoren',1,3,4,'Verstoring van de dienst, zodat andere (overheids)diensten geraakt worden.'),(3,1,'Medewerkers',1,1,1,'Onoplettendheid.');
/*!40000 ALTER TABLE `actors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bia`
--

DROP TABLE IF EXISTS `bia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_id` int(10) unsigned NOT NULL,
  `item` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `impact` text NOT NULL,
  `availability` tinyint(3) unsigned NOT NULL,
  `integrity` tinyint(3) unsigned NOT NULL,
  `confidentiality` tinyint(3) unsigned NOT NULL,
  `owner` tinyint(3) unsigned NOT NULL,
  `location` enum('intern','extern','saas') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`case_id`),
  CONSTRAINT `bia_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bia`
--
-- ORDER BY:  `id`

LOCK TABLES `bia` WRITE;
/*!40000 ALTER TABLE `bia` DISABLE KEYS */;
INSERT INTO `bia` VALUES (1,1,'Fictief Register website','Website waarmee het Fictieve Register aan het grote publiek wordt aangeboden.','Het niet beschikbaar zijn van de website geeft niet direct problemen, maar veelvuldige of langdurige uitval kan het vertrouwen in de organisatie schaden.\r\nFoutieve informatie kan het vertrouwen in de organisatie schaden.',2,3,1,1,'extern'),(2,1,'Fictief Register API-gateway','API waarmee de website en andere partijen de Fictieve Register-database kunnen bevragen.','Vele uitvoeringsorganisaties van de Rijksoverheid en commerciele bedrijven zijn afhankelijk van deze informatie voor de uitvoering van hun werkzaamheden. Verstoring van deze API kan het vertrouwen in de organisatie schaden.\r\nFoutieve informatie kan het vertrouwen in de organisatie schaden.',3,3,1,1,'intern'),(3,1,'Fictief Register database','Database ten behoeve van het Fictieve Register.','Verstoring van de beschikbaarheid van deze informatie kan het vertrouwen in de organisatie schaden.\r\nFoutief informatie kan het vertrouwen in de organisatie schaden.',3,3,1,1,'intern'),(4,1,'Publieke statistieken database','Externe bron met publieke statistieken.','',1,1,1,0,'saas'),(5,1,'Website Analytics','Statistieken over hoe vaak welke gegevens worden opgevraagd.','',1,1,2,0,'extern'),(6,1,'Online Enquête systeem','Online tooling om onderzoeksinformatie te verzamelen.','Inbreuk op de vertrouwelijkheid van de enquêtegegevens kan het vertrouwen in de organisatie schaden.',1,1,3,0,'saas'),(7,1,'Fileserver','De fileserver bevat vertrouwelijke informatie vanuit de enquêtes.','Inbreuk op de vertrouwelijkheid van de enquêtegegevens kan het vertrouwen in de organisatie schaden.',1,3,3,0,'intern');
/*!40000 ALTER TABLE `bia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache` (
  `key` varchar(100) NOT NULL,
  `value` mediumtext NOT NULL,
  `timeout` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_bia_threat`
--

DROP TABLE IF EXISTS `case_bia_threat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_bia_threat` (
  `case_id` int(10) unsigned NOT NULL,
  `bia_id` int(10) unsigned NOT NULL,
  `threat_id` int(10) unsigned NOT NULL,
  KEY `bia_id` (`bia_id`),
  KEY `threat_id` (`threat_id`),
  KEY `case_id` (`case_id`),
  CONSTRAINT `case_bia_threat_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`),
  CONSTRAINT `case_bia_threat_ibfk_2` FOREIGN KEY (`bia_id`) REFERENCES `bia` (`id`),
  CONSTRAINT `case_bia_threat_ibfk_3` FOREIGN KEY (`threat_id`) REFERENCES `threats` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_bia_threat`
--

LOCK TABLES `case_bia_threat` WRITE;
/*!40000 ALTER TABLE `case_bia_threat` DISABLE KEYS */;
INSERT INTO `case_bia_threat` VALUES (1,2,14),(1,3,14),(1,6,14),(1,2,13),(1,3,13),(1,6,13),(1,7,13),(1,1,13),(1,7,14),(1,1,14),(1,2,15),(1,3,15),(1,6,15),(1,7,15),(1,1,15),(1,7,17),(1,6,17),(1,7,19),(1,6,19),(1,7,20),(1,6,20),(1,6,23),(1,7,23),(1,3,23),(1,7,24),(1,6,24),(1,3,24),(1,6,26),(1,7,26),(1,1,28),(1,2,28),(1,3,29),(1,1,29),(1,2,29),(1,6,58),(1,7,30),(1,6,30),(1,6,32),(1,7,32),(1,6,33),(1,7,33),(1,6,36),(1,3,36),(1,4,36),(1,7,38),(1,3,38),(1,2,39),(1,1,39),(1,3,39),(1,7,42),(1,3,42),(1,6,42),(1,1,57),(1,2,57),(1,3,57),(1,7,44),(1,6,44),(1,2,49),(1,3,49),(1,1,49),(1,6,52),(1,4,52),(1,1,53),(1,2,53),(1,3,53);
/*!40000 ALTER TABLE `case_bia_threat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `case_threat`
--

DROP TABLE IF EXISTS `case_threat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_threat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_id` int(10) unsigned NOT NULL,
  `threat_id` int(10) unsigned NOT NULL,
  `chance` tinyint(4) DEFAULT NULL,
  `impact` tinyint(4) DEFAULT NULL,
  `handle` tinyint(4) DEFAULT NULL,
  `action` text,
  `current` text,
  `argumentation` text,
  PRIMARY KEY (`id`),
  KEY `threat_id` (`threat_id`),
  KEY `case_id` (`case_id`),
  CONSTRAINT `case_threat_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`),
  CONSTRAINT `case_threat_ibfk_2` FOREIGN KEY (`threat_id`) REFERENCES `threats` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `case_threat`
--
-- ORDER BY:  `id`

LOCK TABLES `case_threat` WRITE;
/*!40000 ALTER TABLE `case_threat` DISABLE KEYS */;
INSERT INTO `case_threat` VALUES (1,1,1,2,2,1,NULL,NULL,NULL),(2,1,2,2,2,1,NULL,NULL,NULL),(3,1,3,3,2,1,NULL,NULL,NULL),(4,1,45,1,2,4,NULL,NULL,NULL),(5,1,46,1,1,4,NULL,NULL,NULL),(6,1,48,1,1,4,NULL,NULL,NULL),(7,1,49,2,3,1,NULL,NULL,NULL),(8,1,50,1,1,4,NULL,NULL,NULL),(10,1,52,1,3,3,NULL,NULL,NULL),(11,1,43,1,1,4,NULL,NULL,NULL),(12,1,53,1,2,3,NULL,NULL,NULL),(13,1,47,1,1,4,NULL,NULL,NULL),(14,1,44,3,1,2,NULL,NULL,NULL),(15,1,57,2,3,1,NULL,NULL,NULL),(16,1,42,2,2,2,NULL,NULL,NULL),(17,1,41,2,2,4,NULL,NULL,NULL),(18,1,40,1,2,4,NULL,NULL,NULL),(19,1,39,3,2,2,NULL,NULL,NULL),(20,1,56,1,1,4,NULL,NULL,NULL),(21,1,37,1,1,4,NULL,NULL,NULL),(22,1,38,2,2,3,NULL,NULL,NULL),(23,1,36,1,2,3,NULL,NULL,NULL),(24,1,35,1,1,4,NULL,NULL,NULL),(26,1,33,2,3,2,NULL,NULL,NULL),(27,1,32,4,2,2,NULL,NULL,NULL),(28,1,20,4,1,2,NULL,NULL,NULL),(29,1,21,1,1,4,NULL,NULL,NULL),(30,1,22,1,1,4,NULL,NULL,NULL),(31,1,23,2,5,1,NULL,NULL,NULL),(32,1,24,3,2,2,NULL,NULL,NULL),(33,1,25,1,1,4,NULL,NULL,NULL),(34,1,26,2,4,2,NULL,NULL,NULL),(35,1,16,1,2,4,NULL,NULL,NULL),(36,1,17,1,2,2,NULL,NULL,NULL),(37,1,18,1,1,4,NULL,NULL,NULL),(38,1,19,1,2,2,NULL,NULL,NULL),(39,1,11,2,2,4,NULL,NULL,NULL),(40,1,12,1,1,4,NULL,NULL,NULL),(41,1,13,2,4,1,NULL,NULL,NULL),(42,1,14,2,3,1,NULL,NULL,NULL),(43,1,15,2,2,1,NULL,NULL,NULL),(44,1,27,1,1,4,NULL,NULL,NULL),(45,1,31,1,1,4,NULL,NULL,NULL),(46,1,30,3,3,2,NULL,NULL,NULL),(47,1,58,1,1,1,NULL,NULL,NULL),(48,1,29,1,2,4,NULL,NULL,NULL),(49,1,28,2,3,1,NULL,NULL,NULL),(53,1,7,1,1,4,NULL,NULL,NULL),(54,1,8,1,1,4,NULL,NULL,NULL),(55,1,9,1,1,4,NULL,NULL,NULL),(58,1,60,3,4,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `case_threat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases`
--

DROP TABLE IF EXISTS `cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `iso_standard_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `organisation` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `scope` text NOT NULL,
  `impact` text NOT NULL,
  `logo` varchar(250) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_standard_id` (`iso_standard_id`),
  KEY `organisation_id` (`organisation_id`),
  CONSTRAINT `cases_ibfk_2` FOREIGN KEY (`iso_standard_id`) REFERENCES `iso_standards` (`id`),
  CONSTRAINT `cases_ibfk_3` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases`
--
-- ORDER BY:  `id`

LOCK TABLES `cases` WRITE;
/*!40000 ALTER TABLE `cases` DISABLE KEYS */;
INSERT INTO `cases` VALUES (1,1,2,'Registerafdeling','Fictief Register Nederland','2019-01-01','de afdeling die het Fictief Register beheert en exploiteert.','[\"Financieel: < \\u20ac1.000 \\/ Imago: individueel\",\"Financieel: \\u20ac1.000 - \\u20ac10.000 \\/ Imago: persoonlijke kring\",\"Financieel: \\u20ac10.000 - \\u20ac100.000 \\/ Imago: plaatselijke pers\",\"Financieel: \\u20ac100.000 - \\u20ac1.000.000 \\/ Imago: regionale pers\",\"Financieel: > \\u20ac1.000.000 \\/ Imago: landelijke pers\"]','https://www.ravib.nl/images/layout/ravib_logo.png',1);
/*!40000 ALTER TABLE `cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controls`
--

DROP TABLE IF EXISTS `controls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controls` (
  `iso_measure_id` int(10) unsigned NOT NULL,
  `threat_id` int(10) unsigned NOT NULL,
  KEY `iso_measure_id` (`iso_measure_id`),
  KEY `threat_id` (`threat_id`),
  CONSTRAINT `controls_ibfk_1` FOREIGN KEY (`iso_measure_id`) REFERENCES `iso_measures` (`id`),
  CONSTRAINT `controls_ibfk_2` FOREIGN KEY (`threat_id`) REFERENCES `threats` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controls`
--

LOCK TABLES `controls` WRITE;
/*!40000 ALTER TABLE `controls` DISABLE KEYS */;
INSERT INTO `controls` VALUES (6,41),(7,25),(9,29),(17,25),(18,25),(19,25),(36,29),(37,41),(38,24),(38,29),(40,24),(40,25),(42,41),(49,41),(52,35),(52,41),(53,29),(54,24),(57,25),(58,25),(59,25),(60,25),(61,25),(62,25),(63,29),(73,24),(73,25),(74,25),(76,23),(78,23),(79,24),(80,24),(81,25),(84,29),(85,29),(86,29),(87,29),(88,24),(89,24),(90,23),(92,24),(93,24),(96,24),(97,24),(98,29),(98,41),(105,41),(108,41),(113,29),(126,35),(127,24),(38,38),(51,38),(53,38),(54,38),(82,38),(83,38),(85,38),(86,38),(87,38),(96,38),(97,38),(28,31),(29,31),(30,31),(32,31),(34,31),(36,31),(51,31),(103,27),(104,27),(129,27),(8,22),(16,22),(20,22),(21,22),(24,22),(32,22),(43,22),(44,22),(67,22),(68,22),(70,22),(75,22),(91,22),(127,22),(16,37),(38,37),(40,37),(57,37),(96,37),(103,37),(8,17),(14,17),(15,17),(24,17),(26,17),(28,17),(29,17),(30,17),(32,17),(33,17),(34,17),(40,17),(52,17),(55,17),(38,30),(40,30),(73,30),(82,30),(96,30),(97,30),(103,30),(24,20),(27,20),(38,20),(73,20),(76,20),(78,20),(79,20),(80,20),(82,20),(88,20),(89,20),(90,20),(92,20),(93,20),(104,20),(26,44),(28,44),(29,44),(30,44),(32,44),(33,44),(34,44),(36,44),(45,44),(95,44),(96,44),(97,44),(9,11),(51,11),(52,11),(64,11),(65,11),(85,11),(86,11),(87,11),(113,11),(6,40),(41,40),(42,40),(44,40),(49,40),(52,40),(54,40),(98,40),(105,40),(108,40),(109,40),(110,40),(112,40),(7,32),(57,32),(58,32),(59,32),(60,32),(61,32),(62,32),(64,32),(65,32),(101,32),(103,32),(111,32),(7,33),(57,33),(58,33),(59,33),(60,33),(61,33),(62,33),(64,33),(65,33),(103,33),(111,33),(11,7),(45,7),(46,7),(47,7),(64,7),(65,7),(103,7),(129,7),(103,9),(104,9),(129,9),(17,8),(18,8),(55,8),(82,8),(83,8),(86,8),(96,8),(97,8),(129,8),(6,39),(42,39),(44,39),(49,39),(98,39),(99,39),(100,39),(101,39),(102,39),(105,39),(108,39),(109,39),(110,39),(112,39),(13,53),(26,53),(41,53),(45,53),(46,53),(47,53),(120,53),(9,36),(59,36),(67,36),(68,36),(126,36),(8,45),(28,45),(31,45),(52,45),(119,45),(120,45),(121,45),(122,45),(123,45),(8,48),(28,48),(31,48),(52,48),(119,48),(120,48),(121,48),(122,48),(123,48),(8,50),(28,50),(31,50),(33,50),(34,50),(35,50),(36,50),(8,47),(28,47),(31,47),(119,47),(120,47),(121,47),(122,47),(123,47),(8,46),(31,46),(52,46),(119,46),(120,46),(121,46),(122,46),(123,46),(31,49),(35,49),(36,49),(98,49),(120,49),(8,16),(16,16),(20,16),(21,16),(22,16),(24,16),(44,16),(68,16),(70,16),(128,16),(130,16),(105,43),(113,43),(120,43),(11,52),(13,52),(45,52),(46,52),(47,52),(48,52),(64,52),(65,52),(112,52),(120,52),(7,19),(11,19),(12,19),(13,19),(19,19),(20,19),(22,19),(45,19),(46,19),(47,19),(67,19),(68,19),(69,19),(70,19),(112,19),(132,19),(133,19),(6,56),(41,56),(42,56),(47,56),(101,56),(102,56),(108,56),(109,56),(110,56),(112,56),(6,28),(9,28),(51,28),(64,28),(65,28),(85,28),(86,28),(87,28),(98,28),(99,28),(100,28),(101,28),(102,28),(113,28),(7,42),(16,42),(21,42),(22,42),(24,42),(49,42),(52,42),(108,42),(15,13),(71,13),(114,13),(116,13),(15,14),(67,14),(68,14),(69,14),(70,14),(72,14),(88,14),(117,14),(118,14),(119,14),(120,14),(121,14),(6,15),(15,15),(41,15),(42,15),(71,15),(114,15),(116,15),(117,15),(130,15),(131,15),(38,26),(39,26),(55,26),(56,26),(48,12),(99,12),(17,21),(18,21),(19,21),(27,21),(44,21),(45,21),(46,21),(47,21),(73,21),(75,21),(76,21),(77,21),(81,21),(94,21),(126,21),(127,21),(133,21),(1,1),(2,1),(1,2),(2,2),(3,2),(4,2),(5,2),(19,2),(114,2),(116,2),(130,2),(131,2),(3,3),(4,3),(7,3),(16,3),(19,3),(21,3),(22,3),(24,3),(79,3),(80,3),(23,3),(23,16),(23,23),(23,24),(23,32),(23,33),(23,37),(23,42),(25,17),(25,20),(25,21),(25,44),(107,21),(107,22),(107,28),(107,39),(50,11),(50,28),(50,31),(50,38),(125,7),(125,8),(124,7),(124,8),(124,9),(115,13),(115,15),(106,21),(106,25),(8,18),(21,18),(22,18),(24,18),(107,57),(110,57),(112,57),(11,58),(13,58),(46,58),(47,58),(119,58),(130,58),(174,27),(175,27),(245,27),(145,18),(146,18),(141,42),(147,42),(164,42),(196,42),(182,41),(185,41),(196,41),(227,41),(240,41),(141,56),(151,56),(192,56),(201,56),(202,56),(212,56),(216,56),(218,56),(221,56),(229,56),(227,7),(228,7),(229,7),(241,7),(242,7),(244,7),(174,9),(175,9),(241,9),(245,9),(237,47),(238,47),(239,47),(176,44),(177,44),(178,44),(179,44),(180,44),(181,44),(182,44),(190,44),(142,38),(143,38),(195,38),(205,38),(206,38),(207,38),(225,38),(226,38),(145,16),(147,16),(152,16),(197,16),(201,16),(203,16),(247,16),(144,20),(147,20),(162,20),(164,20),(165,20),(171,20),(189,20),(190,20),(209,20),(210,20),(214,20),(230,15),(231,15),(235,15),(247,15),(176,45),(177,45),(178,45),(179,45),(182,45),(190,45),(196,45),(237,45),(238,45),(239,45),(240,45),(176,48),(179,48),(182,48),(190,48),(196,48),(237,48),(238,48),(239,48),(240,48),(176,46),(179,46),(182,46),(190,46),(196,46),(237,46),(238,46),(239,46),(240,46),(184,49),(240,49),(159,33),(174,33),(208,33),(210,33),(211,33),(213,33),(179,50),(182,50),(184,50),(191,53),(229,53),(142,37),(157,37),(159,37),(174,37),(187,37),(211,37),(176,31),(177,31),(181,31),(182,31),(184,31),(195,31),(203,31),(242,31),(143,30),(186,30),(187,30),(141,58),(204,58),(225,58),(226,58),(227,58),(228,58),(229,58),(140,29),(170,29),(184,29),(202,29),(205,29),(207,29),(222,29),(223,29),(227,29),(228,29),(140,28),(195,28),(202,28),(207,28),(212,28),(216,28),(217,28),(218,28),(219,28),(222,28),(223,28),(227,28),(228,28),(248,28),(147,23),(165,23),(168,23),(170,23),(171,23),(147,24),(187,24),(189,24),(190,24),(206,24),(151,26),(157,26),(158,26),(159,26),(186,26),(188,26),(141,43),(191,43),(212,43),(221,43),(229,43),(195,13),(197,13),(230,13),(231,13),(232,13),(233,13),(234,13),(147,11),(160,11),(161,11),(165,11),(168,11),(195,11),(196,11),(202,11),(203,11),(207,11),(227,11),(248,11),(146,1),(151,1),(237,1),(246,1),(247,1),(144,19),(178,19),(211,19),(225,19),(226,19),(141,60),(191,60),(192,60),(193,60),(216,60),(217,60),(218,60),(219,60),(225,60),(226,60),(227,60),(228,60),(229,60),(173,57),(194,57),(215,57),(220,57),(221,57),(222,57),(223,57),(224,57),(191,40),(192,40),(204,40),(212,40),(216,40),(217,40),(218,40),(227,40),(194,39),(201,39),(212,39),(216,39),(217,39),(218,39),(219,39),(222,39),(223,39),(227,39),(193,12),(223,12),(226,12),(240,12),(138,22),(144,22),(149,22),(152,22),(162,22),(164,22),(199,22),(200,22),(211,22),(225,22),(226,22),(148,16),(148,18),(148,20),(148,22),(148,15),(148,32),(154,32),(155,32),(157,32),(159,32),(174,32),(208,32),(209,32),(210,32),(211,32),(213,32),(214,32),(227,32),(244,32),(142,17),(144,17),(150,17),(153,17),(177,17),(178,17),(179,17),(181,17),(182,17),(186,17),(187,17),(196,17),(135,1),(136,1),(142,8),(143,8),(147,8),(241,8),(242,8),(245,8),(198,16),(198,20),(198,28),(137,3),(144,3),(145,3),(146,3),(147,3),(148,3),(150,14),(197,14),(198,14),(199,14),(200,14),(233,14),(234,14),(235,14),(236,14),(237,14),(238,14),(149,21),(151,21),(153,21),(160,21),(161,21),(162,21),(163,21),(164,21),(165,21),(166,21),(167,21),(169,21),(172,21),(173,21),(194,21),(151,25),(152,25),(154,25),(155,25),(156,25),(160,25),(161,25),(208,25),(213,25),(224,25),(243,25),(174,36),(208,36),(209,36),(213,36),(214,36),(225,36),(157,35),(196,35),(203,35),(243,35),(183,45),(183,46),(183,49),(183,50),(137,2),(139,2),(146,2),(147,2),(150,2),(152,2),(154,2),(246,2),(247,2),(141,52),(191,52),(212,52),(229,52),(240,52);
/*!40000 ALTER TABLE `controls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `iso_measure_categories`
--

DROP TABLE IF EXISTS `iso_measure_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iso_measure_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iso_standard_id` int(10) unsigned NOT NULL,
  `number` tinyint(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_standard` (`iso_standard_id`),
  CONSTRAINT `iso_measure_categories_ibfk_1` FOREIGN KEY (`iso_standard_id`) REFERENCES `iso_standards` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iso_measure_categories`
--
-- ORDER BY:  `id`

LOCK TABLES `iso_measure_categories` WRITE;
/*!40000 ALTER TABLE `iso_measure_categories` DISABLE KEYS */;
INSERT INTO `iso_measure_categories` VALUES (3,2,5,'Informatiebeveiligingsbeleid'),(4,2,6,'Organiseren van informatiebeveiliging'),(5,2,7,'Veilig personeel'),(6,2,8,'Beheer van bedrijfsmiddelen'),(7,2,9,'Toegangsbeveiliging'),(8,2,10,'Cryptografie'),(9,2,11,'Fysieke beveiliging en beveiliging van de omgeving'),(10,2,12,'Beveiliging bedrijfsvoering'),(11,2,13,'Communicatiebeveiliging'),(12,2,14,'Acquisitie, ontwikkeling en onderhoud van informatiesystemen'),(13,2,15,'Leveranciersrelaties'),(14,2,16,'Beheer van informatiebeveiligingsincidenten'),(15,2,17,'Informatiebeveiligingsaspecten van bedrijfscontiniuïteitsbeheer'),(16,2,18,'Naleving'),(17,1,5,'Beveiliigingsbeleid'),(18,1,6,'Organisatie van informatiebeveiliging'),(19,1,7,'Beheer van bedrijfsmiddelen'),(20,1,8,'Beveiliging van personeel'),(21,1,9,'Fysieke beveiliging en beveiliging van de omgeving'),(22,1,10,'Beheer van communicatie- en bedieningsprocessen'),(23,1,11,'Toegangsbeveiliging'),(24,1,12,'Verwerving, ontwikkeling en onderhoud van informatiesystemen'),(25,1,13,'Beheer van informatiebeveiligingsincidenten'),(26,1,14,'Bedrijfscontinuïteitsbeheer'),(27,1,15,'Naleving');
/*!40000 ALTER TABLE `iso_measure_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `iso_measures`
--

DROP TABLE IF EXISTS `iso_measures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iso_measures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iso_standard_id` int(10) unsigned NOT NULL,
  `number` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `reduce` tinyint(4) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_standard` (`iso_standard_id`),
  CONSTRAINT `iso_measures_ibfk_1` FOREIGN KEY (`iso_standard_id`) REFERENCES `iso_standards` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iso_measures`
--
-- ORDER BY:  `id`

LOCK TABLES `iso_measures` WRITE;
/*!40000 ALTER TABLE `iso_measures` DISABLE KEYS */;
INSERT INTO `iso_measures` VALUES (1,1,'5.1.1','Beleidsdocument voor informatiebeveiliging',2),(2,1,'5.1.2','Beoordeling van het informatiebeveiligingsbeleid',2),(3,1,'6.1.1','Betrokkenheid van de directie bij informatiebeveiliging',2),(4,1,'6.1.2','Coördinatie van informatiebeveiliging',2),(5,1,'6.1.3','Toewijzing van verantwoordelijkheden voor informatiebeveiliging',2),(6,1,'6.1.4','Goedkeuringsproces voor IT-voorzieningen',2),(7,1,'6.1.5','Geheimhoudingsovereenkomst',2),(8,1,'6.1.6','Contact met overheidsinstanties',2),(9,1,'6.1.7','Contact met speciale belangengroepen',2),(10,1,'6.1.8','Onafhankelijke beoordeling van informatiebeveiliging',2),(11,1,'6.2.1','Identificatie van risico\'s die betrekking hebben op externe partijen',2),(12,1,'6.2.2','Beveiliging behandelen in de omgang met klanten',2),(13,1,'6.2.3','Beveiliging behandelen in overeenkomsten met een derde partij',2),(14,1,'7.1.1','Inventarisatie van bedrijfsmiddelen',2),(15,1,'7.1.2','Eigendom van bedrijfsmiddelen',2),(16,1,'7.1.3','Aanvaardbaar gebruik van bedrijfsmiddelen',2),(17,1,'7.2.1','Richtlijnen voor classificatie',2),(18,1,'7.2.2','Labeling en verwerking van informatie',2),(19,1,'8.1.1','Rollen en verantwoordelijkheden',2),(20,1,'8.1.2','Screening',2),(21,1,'8.1.3','Arbeidsvoorwaarden',2),(22,1,'8.2.1','Verantwoordelijkheden van de directie',2),(23,1,'8.2.2','Bewustmaking, opleiding en training in informatiebeveiliging',2),(24,1,'8.2.3','Disciplinaire maatregelen',2),(25,1,'8.3.1','Beëindigingsverantwoordelijkheden',2),(26,1,'8.3.2','Teruggave van bedrijfsmiddelen',2),(27,1,'8.3.3','Intrekken van toegangsrechten',2),(28,1,'9.1.1','Fysieke beveiliging van de omgeving',2),(29,1,'9.1.2','Fysieke toegangsbeveiliging',2),(30,1,'9.1.3','Beveiliging van kantoren, ruimten en faciliteiten',2),(31,1,'9.1.4','Bescherming tegen bedreigingen van buitenaf',2),(32,1,'9.1.5','Werken in beveiligde ruimten',2),(33,1,'9.1.6','Openbare toegang en gebieden voor laden en lossen',2),(34,1,'9.2.1','Plaatsing en beveiliging van apparatuur',2),(35,1,'9.2.2','Nutsvoorzieningen',2),(36,1,'9.2.3','Beveiliging van kabels',2),(37,1,'9.2.4','Onderhoud van apparatuur',2),(38,1,'9.2.5','Beveiliging van apparatuur buiten het terrein',2),(39,1,'9.2.6','Veilig verwijderen en hergebruiken van apparatuur',2),(40,1,'9.2.7','Verwijdering van bedrijfseigendommen',2),(41,1,'10.1.1','Gedocumenteerde bedieningsprocedures',2),(42,1,'10.1.2','Wijzigingsbeheer',2),(43,1,'10.1.3','Functiescheiding',2),(44,1,'10.1.4','Scheiding van voorzieningen voor ontwikkeling, testen en productie',2),(45,1,'10.2.1','Dienstverlening',2),(46,1,'10.2.2','Controle en beoordeling van dienstverlening door een derde partij',2),(47,1,'10.2.3','Beheer van wijzigingen in dienstverlening door een derde partij',2),(48,1,'10.3.1','Capaciteitsbeheer',2),(49,1,'10.3.2','Systeemacceptatie',2),(50,1,'10.4.1','Maatregelen tegen virussen',2),(51,1,'10.4.2','Maatregelen tegen \'mobile code\'',2),(52,1,'10.5.1','Reservekopieën maken (back-ups)',2),(53,1,'10.6.1','Maatregelen voor netwerken',2),(54,1,'10.6.2','Beveiliging van netwerkdiensten',2),(55,1,'10.7.1','Beheer van verwijderbare media',2),(56,1,'10.7.2','Verwijdering van media',2),(57,1,'10.7.3','Procedures voor de behandeling van informatie',2),(58,1,'10.7.4','Beveiliging van systeemdocumentatie',2),(59,1,'10.8.1','Beleid en procedures voor informatie-uitwisseling',2),(60,1,'10.8.2','Uitwisselingsovereenkomsten',2),(61,1,'10.8.3','Fysieke media die worden getransporteerd',2),(62,1,'10.8.4','Elektronisch berichtenuitwisseling',2),(63,1,'10.8.5','Systemen voor bedrijfsinformatie',2),(64,1,'10.9.1','E-commerce',2),(65,1,'10.9.2','Online transacties',2),(66,1,'10.9.3','Openbaar beschikbare informatie',2),(67,1,'10.10.1','Aanmaken van auditlogboeken',2),(68,1,'10.10.2','Controle van systeemgebruik',2),(69,1,'10.10.3','Bescherming van informatie in logbestanden',2),(70,1,'10.10.4','Logbestanden van administrators en operators',2),(71,1,'10.10.5','Registratie van storingen',2),(72,1,'10.10.6','Synchronisatie van systeemklokken',2),(73,1,'11.1.1','Toegangsbeleid',2),(74,1,'11.2.1','Registratie van gebruikers',2),(75,1,'11.2.2','Beheer van speciale bevoegdheden',2),(76,1,'11.2.3','Beheer van gebruikerswachtwoorden',2),(77,1,'11.2.4','Beoordeling van toegangsrechten van gebruikers',2),(78,1,'11.3.1','Gebruik van wachtwoorden',2),(79,1,'11.3.2','Onbeheerde gebruikersapparatuur',2),(80,1,'11.3.3','\'Clear desk\'- en \'clear screen\'-beleid',2),(81,1,'11.4.1','Beleid ten aanzien van het gebruik van netwerkdiensten',2),(82,1,'11.4.2','Authenticatie van gebruikers bij externe verbindingen',2),(83,1,'11.4.3','Identificatie van netwerkapparatuur',2),(84,1,'11.4.4','Bescherming op afstand van poorten voor diagnose en configuratie',2),(85,1,'11.4.5','Scheiding van netwerken',2),(86,1,'11.4.6','Beheersmaatregelen voor netwerkverbindingen',2),(87,1,'11.4.7','Beheersmaatregelen voor netwerkroutering',2),(88,1,'11.5.1','Beveiligde inlogprocedures',2),(89,1,'11.5.2','Gebruikersindentificatie en -authenticatie',2),(90,1,'11.5.3','Systemen voor wachtwoordbeheer',2),(91,1,'11.5.4','Gebruik van systeemhulpmiddelen',2),(92,1,'11.5.5','Time-out van sessies',2),(93,1,'11.5.6','Beperking van de verbindingstijd',2),(94,1,'11.6.1','Beperken van toegang tot informatie',2),(95,1,'11.6.2','Isoleren van gevoelige systemen',2),(96,1,'11.7.1','Draagbare computers en communicatievoorzieningen',2),(97,1,'11.7.2','Telewerken',2),(98,1,'12.1.1','Analyse en specificatie van beveiligingseisen',2),(99,1,'12.2.1','Validatie van invoergegevens',2),(100,1,'12.2.2','Beheersing van interne gegevensverwerking',2),(101,1,'12.2.3','Integriteit van berichten',2),(102,1,'12.2.4','Validatie van uitvoergegevens',2),(103,1,'12.3.1','Beleid voor het gebruik van cryptografische beheersmaatregelen',2),(104,1,'12.3.2','Sleutelbeheer',2),(105,1,'12.4.1','Beheersing van operationele programmatuur',2),(106,1,'12.4.2','Bescherming van testdata',2),(107,1,'12.4.3','Toegangsbeheersing voor broncode van programmatuur',2),(108,1,'12.5.1','Procedures voor wijzigingsbeheer',2),(109,1,'12.5.2','Technische beoordeling van toepassingen na wijzigingen in het besturingssysteem',2),(110,1,'12.5.3','Restricties op wijzigingen in programmatuurpakketten',2),(111,1,'12.5.4','Uitlekken van informatie',2),(112,1,'12.5.5','Uitbestede ontwikkeling van programmatuur',2),(113,1,'12.6.1','Beheersing van technische kwetsbaarheden',2),(114,1,'13.1.1','Rapportage van informatiebeveiligingsgebeurtenissen',2),(115,1,'13.1.2','Rapportage van zwakke plekken in de beveiliging',2),(116,1,'13.2.1','Verantwoordelijkheden en procedures',2),(117,1,'13.2.2','Leren van informatiebeveiligingsincidenten',2),(118,1,'13.2.3','Verzamelen van bewijsmateriaal',2),(119,1,'14.1.1','Informatiebeveiliging opnemen in het proces van bedrijfscontinuïteitsbeheer',2),(120,1,'14.1.2','Bedrijfscontinuïteit en risicobeoordeling',2),(121,1,'14.1.3','Continuïteitsplannen ontwikkelen en implementeren waaronder informatiebeveiliging',2),(122,1,'14.1.4','Kader voor de bedrijfscontinuïteitsplanning',2),(123,1,'14.1.5','Testen, onderhoud en herbeoordelen van bedrijfscontinuïteitsplannen',2),(124,1,'15.1.1','Identificatie van toepasselijke wetgeving',2),(125,1,'15.1.2','Intellectuele eigendomsrechten (Intellectual Property Rights, IPR)',2),(126,1,'15.1.3','Bescherming van bedrijfsdocumenten',2),(127,1,'15.1.4','Bescherming van gegevens en geheimhouding van persoonsgegevens',2),(128,1,'15.1.5','Voorkomen van misbruik van IT-voorzieningen',2),(129,1,'15.1.6','Voorschriften voor het gebruik van cryptografische beheersmaatregelen',2),(130,1,'15.2.1','Naleving van beveiligingsbeleid en -normen',2),(131,1,'15.2.2','Controle op technische naleving',2),(132,1,'15.3.1','Beheersmaatregelen voor audits van informatiesystemen',2),(133,1,'15.3.2','Bescherming van hulpmiddelen voor audits van informatiesystemen',2),(135,2,'5.1.1','Beleidsregels voor informatiebeveiliging',2),(136,2,'5.1.2','Beoordeling van het informatiebeveiligingsbeleid',2),(137,2,'6.1.1','Rollen en verantwoordelijkheden bij informatiebeveiliging',2),(138,2,'6.1.2','Scheiding van taken',2),(139,2,'6.1.3','Contact met overheidsinstanties',2),(140,2,'6.1.4','Contact met speciale belangengroepen',2),(141,2,'6.1.5','Informatiebeveiliging in projectbeheer',2),(142,2,'6.2.1','Beleid voor mobiele apparatuur',2),(143,2,'6.2.2','Telewerken',2),(144,2,'7.1.1','Screening',0),(145,2,'7.1.2','Arbeidsvoorwaarden',2),(146,2,'7.2.1','Directieverantwoordelijkheden',2),(147,2,'7.2.2','Bewustzijn, opleiding en training ten aanzien van informatiebeveiliging',2),(148,2,'7.2.3','Disciplinaire procedure',0),(149,2,'7.3.1','Beëindiging of wijziging van verantwoordelijkheden van het dienstverband',0),(150,2,'8.1.1','Inventariseren van bedrijfsmiddelen',2),(151,2,'8.1.2','Eigendom van bedrijfsmiddelen',2),(152,2,'8.1.3','Aanvaardbaar gebruik van bedrijfsmiddelen',0),(153,2,'8.1.4','Teruggeven van bedrijfsmiddelen',0),(154,2,'8.2.1','Classificatie van informatie',0),(155,2,'8.2.2','Informatie labelen',0),(156,2,'8.2.3','Behandelen van bedrijfsmiddelen',0),(157,2,'8.3.1','Beheer van verwijderbare media',0),(158,2,'8.3.2','Verwijderen van media',0),(159,2,'8.3.3','Media fysiek overdragen',2),(160,2,'9.1.1','Beleid voor toegangsbeveiliging',2),(161,2,'9.1.2','Toegang tot netwerken en netwerkdiensten',2),(162,2,'9.2.1','Registratie en afmelden van gebruikers',0),(163,2,'9.2.2','Gebruikers toegang verlenen',0),(164,2,'9.2.3','Beheren van speciale toegangsrechten',0),(165,2,'9.2.4','Beheer van geheime authenticatie-informatie van gebruikers',0),(166,2,'9.2.5','Beoordeling van toegangsrechten van gebruikers',0),(167,2,'9.2.6','Toegangsrechten intrekken of aanpassen',0),(168,2,'9.3.1','Geheime authenticatie-informatie gebruiken',0),(169,2,'9.4.1','Beperking toegang tot informatie',2),(170,2,'9.4.2','Beveiligde inlogprocedures',2),(171,2,'9.4.3','Systeem voor wachtwoordbeheer',0),(172,2,'9.4.4','Speciale systeemhulpmiddelen gebruiken',2),(173,2,'9.4.5','Toegangsbeveiliging op programmabroncode',2),(174,2,'10.1.1','Beleid inzake het gebruik van cryptografische beheersmaatregelen',2),(175,2,'10.1.2','Sleutelbeheer',2),(176,2,'11.1.1','Fysieke beveiligingszone',2),(177,2,'11.1.2','Fysieke toegangsbeveiliging',2),(178,2,'11.1.3','Kantoren, ruimten en faciliteiten beveiligen',2),(179,2,'11.1.4','Beschermen tegen bedreigingen van buitenaf',1),(180,2,'11.1.5','Werken in beveiligde gebieden',0),(181,2,'11.1.6','Laad- en loslocatie',0),(182,2,'11.2.1','Plaatsing en bescherming van apparatuur',0),(183,2,'11.2.2','Nutsvoorzieningen',0),(184,2,'11.2.3','Beveiliging van bekabeling',0),(185,2,'11.2.4','Onderhoud van apparatuur',0),(186,2,'11.2.5','Verwijdering van bedrijfsmiddelen',0),(187,2,'11.2.6','Beveiliging van apparatuur en bedrijfsmiddelen buiten het terrein',2),(188,2,'11.2.7','Veilig verwijderen of hergebruiken van apparatuur',0),(189,2,'11.2.8','Onbeheerde gebruikersapparatuur',0),(190,2,'11.2.9','\'Clear desk\'- en \'clear screen\'-beleid',0),(191,2,'12.1.1','Gedocumenteerde bedieningsprocedures',2),(192,2,'12.1.2','Wijzigingsbeheer',0),(193,2,'12.1.3','Capaciteitsbeheer',0),(194,2,'12.1.4','Scheiding van ontwikkel-, test- en productieomgevingen',0),(195,2,'12.2.1','Beheersmaatregelen tegen malware',0),(196,2,'12.3.1','Back-up van informatie',1),(197,2,'12.4.1','Gebeurtenissen registreren',2),(198,2,'12.4.2','Beschermen van informatie in logbestanden',2),(199,2,'12.4.3','Logbestanden van beheerders en operators',0),(200,2,'12.4.4','Kloksynchronisatie',0),(201,2,'12.5.1','Software installeren op operationele systemen',2),(202,2,'12.6.1','Beheer van technische kwetsbaarheden',2),(203,2,'12.6.2','Beperkingen voor het installeren van software',2),(204,2,'12.7.1','Beheersmaatregelen betreffende audits van informatiesystemen',2),(205,2,'13.1.1','Beheersmaatregelen voor netwerken',2),(206,2,'13.1.2','Beveiliging van netwerkdiensten',2),(207,2,'13.1.3','Scheiding in netwerken',1),(208,2,'13.2.1','Beleid en procedures voor informatietransport',2),(209,2,'13.2.2','Overeenkomsten over informatietransport',2),(210,2,'13.2.3','Elektronische berichten',2),(211,2,'13.2.4','Vertrouwelijkheids- of geheimhoudingsovereenkomst',2),(212,2,'14.1.1','Analyse en specificatie van informatiebeveiligingseisen',2),(213,2,'14.1.2','Toepassingen op openbare netwerken beveiligen',2),(214,2,'14.1.3','Transacties van toepassingen beschermen',2),(215,2,'14.2.1','Beleid voor beveiligd ontwikkelen',2),(216,2,'14.2.2','Procedures voor wijzigingsbeheer met betrekking tot systemen',2),(217,2,'14.2.3','Technische beoordeling van toepassingen na wijzigingen besturingsplatform',0),(218,2,'14.2.4','Beperkingen op wijzigingen aan softwarepakketten',0),(219,2,'14.2.5','Principes voor engineering van beveiligde systemen',2),(220,2,'14.2.6','Beveiligde ontwikkelomgeving',0),(221,2,'14.2.7','Uitbestede softwareontwikkeling',2),(222,2,'14.2.8','Testen van systeembeveiliging',2),(223,2,'14.2.9','Systeemacceptatietests',2),(224,2,'14.3.1','Bescherming van testgegevens',0),(225,2,'15.1.1','Informatiebeveiligingsbeleid voor leveranciersrelaties',2),(226,2,'15.1.2','Opnemen van beveiligingsaspecten in leveranciersovereenkomsten',2),(227,2,'15.1.3','Toeleveringsketen van informatie- en communicatietechnologie',2),(228,2,'15.2.1','Monitoring en beoordeling van dienstverlening van leveranciers',2),(229,2,'15.2.2','Beheer van veranderingen in dienstverlening van leveranciers',2),(230,2,'16.1.1','Verantwoordelijkheden en procedures',2),(231,2,'16.1.2','Rapportage van informatiebeveiligingsgebeurtenissen',2),(232,2,'16.1.3','Rapportage van zwakke plekken in de informatiebeveiliging',0),(233,2,'16.1.4','Beoordeling van en besluitvorming over informatiebeveiligingsgebeurtenissen',1),(234,2,'16.1.5','Respons op informatiebeveiligingsincidenten',1),(235,2,'16.1.6','Lering uit informatiebeveiligingsincidenten',2),(236,2,'16.1.7','Verzamelen van bewijsmateriaal',1),(237,2,'17.1.1','Informatiebeveiligingscontinuïteit plannen',1),(238,2,'17.1.2','Informatiebeveiligingscontinuïteit implementeren',1),(239,2,'17.1.3','Informatiebeveiligingscontinuïteit verifiëren, beoordelen en evalueren',1),(240,2,'17.2.1','Beschikbaarheid van informatieverwerkende faciliteiten',1),(241,2,'18.1.1','Vaststellen van toepasselijke wetgeving en contractuele eisen',2),(242,2,'18.1.2','Intellectuele-eigendomsrechten',2),(243,2,'18.1.3','Beschermen van registraties',2),(244,2,'18.1.4','Privacy en bescherming van persoonsgegevens',2),(245,2,'18.1.5','Voorschriften voor het gebruik van cryptografische beheersmaatregelen',2),(246,2,'18.2.1','Onafhankelijke beoordeling van informatiebeveiliging',2),(247,2,'18.2.2','Naleving van beveiligingsbeleid en -normen',2),(248,2,'18.2.3','Beoordeling van technische naleving',2);
/*!40000 ALTER TABLE `iso_measures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `iso_standards`
--

DROP TABLE IF EXISTS `iso_standards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iso_standards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iso_standards`
--
-- ORDER BY:  `id`

LOCK TABLES `iso_standards` WRITE;
/*!40000 ALTER TABLE `iso_standards` DISABLE KEYS */;
INSERT INTO `iso_standards` VALUES (1,'NEN-ISO/IEC 27002:2007',1),(2,'NEN-ISO/IEC 27002:2017',1);
/*!40000 ALTER TABLE `iso_standards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL,
  `text` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--
-- ORDER BY:  `id`

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,0,'private','private'),(2,1,'Casussen','/casus'),(3,1,'Handleiding','/handleiding'),(4,1,'Risicomatrix','/risicomatrix'),(5,1,'Koppelingen','/koppelingen'),(6,1,'DPIA','/dpia'),(7,1,'Uitloggen','/logout'),(8,0,'public','public'),(9,8,'Inloggen','/casus'),(10,0,'admin','admin'),(11,10,'Website','/casus'),(12,10,'CMS','/admin'),(13,10,'Logout','/logout');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisations`
--

DROP TABLE IF EXISTS `organisations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organisations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `name_2` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisations`
--
-- ORDER BY:  `id`

LOCK TABLES `organisations` WRITE;
/*!40000 ALTER TABLE `organisations` DISABLE KEYS */;
INSERT INTO `organisations` VALUES (1,'RAVIB');
/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `overruled`
--

DROP TABLE IF EXISTS `overruled`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `overruled` (
  `case_id` int(10) unsigned NOT NULL,
  `iso_measure_id` int(10) unsigned NOT NULL,
  KEY `case_id` (`case_id`),
  KEY `iso_measure_id` (`iso_measure_id`),
  CONSTRAINT `overruled_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`),
  CONSTRAINT `overruled_ibfk_2` FOREIGN KEY (`iso_measure_id`) REFERENCES `iso_measures` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_access`
--

DROP TABLE IF EXISTS `page_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_access` (
  `page_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`page_id`,`role_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `page_access_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`),
  CONSTRAINT `page_access_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_access`
--
-- ORDER BY:  `page_id`,`role_id`

LOCK TABLES `page_access` WRITE;
/*!40000 ALTER TABLE `page_access` DISABLE KEYS */;
INSERT INTO `page_access` VALUES (2,2,1);
/*!40000 ALTER TABLE `page_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `language` varchar(2) NOT NULL,
  `layout` varchar(100) DEFAULT NULL,
  `private` tinyint(1) NOT NULL,
  `style` text,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `keywords` varchar(100) NOT NULL,
  `content` mediumtext NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `back` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--
-- ORDER BY:  `id`

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'/homepage','nl',NULL,0,'div.header div.container {\r\n  height:420px;\r\n  background-position:center center;\r\n  background-size:cover;\r\n}\r\n\r\nh1 {\r\n  height:0;\r\n  text-align:center;\r\n  position:relative;\r\n  top:-350px;\r\n  color:#ffffff;\r\n  text-shadow:2px 2px 2px #000000;\r\n  font-size:40px;\r\n  padding:0 5px;\r\n}\r\n\r\n@media (max-width:767px) {\r\n  h1 {\r\n    font-size:28px;\r\n    top:-170px\r\n  }\r\n  div.header div.container {\r\n    height:200px;\r\n  }\r\n}','Risicoanalyse voor informatiebeveiliging','','','<p>Deze website biedt u een hulpmiddel voor het uitvoeren van een risicoanalyse voor informatiebeveiliging en betreft een lokale kopie van de website <a href=\"https://www.ravib.nl/\">www.ravib.nl</a>. Het gebruik van deze website vereist dat u inlogt. Raadpleeg uw beveiligingsfunctionaris, Information Security Officer of systeembeheerder voor een account.</p>',1,0),(2,'/handleiding','nl',NULL,1,NULL,'Handleiding','','','<h2>Inleiding</h2>\r\n<p>De RAVIB website biedt een gratis hulpmiddel voor de uitvoering van een risicoanalyse voor informatiebeveiliging. Hierbij worden, door middel van het doornemen van een lijst met mogelijke dreigingen, maatregelen uit de ISO 27002 standaard, of een standaard die daarvan is afgeleid, geselecteerd die doorgevoerd kunnen worden om de vastgestelde dreigingen tegen te gaan.</p>\r\n\r\n<p>Zo’n hulpmiddel is echter niet veel waard als het niet op de juiste wijze wordt ingezet. Het proces van de risicoanalyse is namelijk belangrijker dan het hulpmiddel dat daarbij ingezet wordt. Om RAVIB op de juiste wijze in te kunnen zetten is deze handleiding geschreven. De beschreven stappen zijn: de voorbereiding; een eerste bijeenkomst; de daadwerkelijke risicoanalyse; het doornemen van de geselecteerde maatregelen en het verwerken van de resultaten.</p>\r\n\r\n<p>Deze handleiding is bedoeld voor degene die de risicoanalyse gaat begeleiden. Dit kan bijvoorbeeld een information security officer of een security consultant zijn. Van deze persoon wordt in ieder geval verwacht dat hij/zij voldoende kennis heeft van informatiebeveiliging in het algemeen.</p>\r\n\r\n<h2>De voorbereiding</h2>\r\n<p>De eerste stap die genomen moet worden voordat men kan beginnen aan een risicoanalyse is het bepalen van de scope. Zeker bij grote organisaties is het ondoenlijk om één risicoanalyse voor de gehele organisatie uit te voeren. Het dan verstandiger om meerdere risicoanalyses uit te voeren waarbij je je per risicoanalyse richt op een beperkt onderdeel van de organisatie. Concreet betekent het bepalen van de scope, bepalen welke informatiesystemen je meeneemt in de risicoanalyse. De scope kan bestaan uit de informatiesystemen behorende bij bijvoorbeeld een proces of een afdeling of een om andere reden bij elkaar horende verzameling van informatiesystemen. Wees voorzichtig met het te ruim kiezen van je scope. Het gevaar van een te ruime scope is dat je daardoor niet diep genoeg op belangrijke details ingaat en dus een te oppervlakkig beeld krijgt van de feitelijke risico’s.</p>\r\n\r\n<p>Indien dit de eerste keer is dat je een risicoanalyse uitvoert, richt je dan op de vitale processen van de organisatie. De vitale processen van een organisatie zijn de processen die ervoor zorgen dat de producten of diensten die een organisatie levert, ook daadwerkelijk geleverd kunnen worden. Neem de informatiesystemen van een vitaal proces als scope voor een risicoanalyse. Maak zelf een afweging of bepaalde vitale processen gezamenlijk in een risicoanalyse behandeld kunnen worden.</p>\r\n\r\n<p>Nadat de scope is vastgesteld, dient bepaald te worden met wie de risicoanalyse wordt uitgevoerd. Het is zeer belangrijk om te beseffen dat kennis over risico’s niet kan voortkomen uit een hulpmiddel of een methodiek, maar slechts uit de mensen die aanwezig zijn bij de risicoanalyse. Zij zijn namelijk degene die weten wat er speelt. Een risicoanalyse is niet meer dan een manier om deze kennis op een gestructureerde manier te verzamelen. De risicoanalyse valt of staat bij de selectie van de deelnemers. Ga dus opzoek naar mensen die goed zicht hebben op wat echt belangrijk is voor de organisatie, maar daarbij nog voldoende zicht hebben op wat er speelt op de werkvloer. Ga opzoek naar mensen die verantwoordelijk zijn voor de zaken die binnen de gekozen scope vallen, mensen die direct de nadelen ondervinden van problemen die zich binnen de gekozen scope voordoen. Realiseer je daarbij dat personen die goed zijn in het inschatten van de kans op een incident, niet per se de personen die ook de juiste impact ervan kunnen inschatten en andersom. Vaak zijn mensen uit de business beter in het inschatten van de impact en techneuten beter in het inschatten van de kans. Zorg tevens voor aanwezigheid van het voor de scope verantwoordelijke management, om ervoor te zorgen dat de uitkomst van de risicoanalyse gedragen wordt.</p>\r\n\r\n<p>Voorafgaand aan de risicoanalyse dient aan de waarden voor impact een bedrag gekoppeld te worden. Dit is in RAVIB niet vast ingevuld, omdat dit voor iedere organisatie anders is. Een schadepost van €10.000,- kan voor een kleine onderneming een groot bedrag zijn en voor een multinational een niet noemenswaardig bedrag. Deze waarden kunnen het beste bepaald worden met iemand met gedegen kennis van de financiële situatie van de organisatie. Hierbij is het belangrijk om te beseffen dat deze waarden niet bedoeld zijn om een schadebedrag aan de uiteindelijke risico\'s te koppelen, maar slechts om de impact van een risico goed te kunnen plaatsen ten opzichte van de impact van de andere risico\'s. De impact hoeft per risico dan ook niet met een berekening of harde cijfers aangetoond te worden. Een goed onderbouwd gevoel is voldoende. Een helder ingevulde impact is belangrijk om een risicoanalyse op een later moment te kunnen herhalen en de resultaten te kunnen vergelijken met de eerder uitgevoerde analyse.</p>\r\n\r\n<h2>Een eerste bijeenkomst</h2>\r\n<p>Een goede risicoanalyse doe je niet in tien minuten. De daarvoor benodigde tijd ligt meer in de buurt van een halve tot een gehele dag. De feitelijk benodigde tijd is uiteraard afhankelijk van de gekozen scope, het aantal deelnemers en hun ervaring met het uitvoeren van een risicoanalyse. Het is daarom belangrijk dat de deelnemers goed beseffen wat er van hen verwacht wordt. Spreek met hen het proces door en geef ze een beeld van de vragenlijst die hen te wachten staat. Voer alleen een risicoanalyse uit met mensen die bereid zijn deze hoeveelheid tijd en energie erin te steken, anders is het zonde van de tijd.</p>\r\n\r\n<p>Hoewel de scope van de risicoanalyse een proces of een afdeling kan zijn, voer je de risicoanalyse uit op de daarbij behorende informatie en informatiesystemen. Want hoewel een risico kan voortkomen uit, bijvoorbeeld, een verkeerd ingericht of ontbrekend proces, dient gekeken te worden naar de risico’s ten aanzien van informatie. We hebben het hier ten slotte over informatiebeveiliging en niet over procesbeveiliging.</p>\r\n\r\n<h2>De risicoanalyse</h2>\r\n<p>De daadwerkelijke risicoanalyse bestaat uit drie stappen: de business impact analyse, het in kaart brengen van de actoren en de dreigingsanalyse.</p>\r\n\r\n<h3>Business Impact Analyse</h3>\r\n<p>De eerste stap van de risicoanalyse is het uitvoeren van de business impact analyse (BIA). In deze stap wordt per informatiesysteem bepaald hoe belangrijk dit systeem is voor de organisatie. Bespreek wat de impact is voor de organisatie in het geval van een probleem met de beschikbaarheid, integriteit en/of vertrouwelijkheid van het systeem en de daarin opgeslagen informatie.</p>\r\n\r\n<p>Wellicht is het verstandig om voorafgaand aan de gehele risicoanalyse de namen van de informatiesystemen die binnen de scope vallen, al in te vullen in het BIA overzicht.</p>\r\n\r\n<h3>In kaart brengen van de actoren</h3>\r\n<p>In deze stap worden de actoren in kaart gebracht die een bedreiging vormen voor de informatiebeveiliging. Hun acties kunnen leiden tot een incident door gebrek aan kundigheid of interesse op het gebied van informatiebeveiliging of omdat zij bewust en doelgericht een digitale aanval uitvoeren. De resultaten van deze stap dienen puur ter ondersteuning bij de uitvoering van de dreigingsanalyse.</p>\r\n\r\n<h3>De dreigingsanalyse</h3>\r\n<p>Tijdens de dreigingsanalyse dient voor iedere dreiging bepaald te worden wat de kans op optreden is en welke impact daarbij hoort. Vervolgens dient bepaald te worden hoe omgegaan moet worden met het vastgestelde risico. Daarbij mag de aanpak \'accepteren\' niet gekozen worden indien als impact \'groot\' of \'desastreus\' of als kans \'wekelijks\' of \'dagelijks\' gekozen is. Belangrijk hierbij is dat de maatregelen die de kans of impact terugdringen, mee worden genomen. Met andere woorden, er dient gekeken te worden naar het zogenoemde restrisico. Het doel is namelijk dat uiteindelijk voor iedere dreiging het restrisico geaccepteerd kan worden doordat afdoende maatregelen genomen zijn. Een handige aanpak hierbij is om per risico toch te beginnen met het bespreken van de dreiging, los van de reeds genomen maatregelen, en pas als deze voor iedereen helder en duidelijk is de reeds genomen maatregelen te benoemen. Hierdoor wordt het restrisico makkelijker inzichtelijk en heb je minder kans dat dit restrisico door de reeds genomen maatregelen onterecht wordt afgedaan als zijnde \'verwaarloosbaar klein\'.</p>\r\n\r\n<p>Per dreiging zijn drie invulvelden beschikbaar; \'Gewenste situatie / te nemen acties\', \'Huidige situatie / huidige maatregelen\' en \'Argumentatie voor gemaakte keuze\'. Deze velden kunnen gebruikt worden voor respectievelijk een nulmeting, het latere plan van aanpak en argumentatie over de gekozen kans, impact en aanpak. De argumentatie is belangrijke informatie bij een eventuele certificering. De inhoud van deze velden is daardoor belangrijker dan de kans, impact en aanpak velden. Deze laatste geven in feite niet meer dan een prioritering of urgentie aan.</p>\r\n\r\n<p>Denk bij kans aan of uberhaupt sprake is van de dreiging, de benodigde kennis voor de dreiging, het kennisniveau van de mogelijke aanvaller en de motivatie van de aanvaller om de organisatie aan te vallen.</p>\r\n<p>Denk bij impact aan de mogelijke gevolgen voor de beschikbaarheid, integriteit en vertrouwelijkheid, de imagoschade en financiële schade.<p>\r\n<p>Bij de aanpak betekent \'beheersen\' het tegengaan van zowel de kans als de impact, \'ontwijken\' het tegengaan van de kans, \'verweren\' het tegengaan van de impact en \'accepteren\' het niet nemen van actie om het risico te verlagen.</p>\r\n\r\n<h2>Doornemen van geselecteerde maatregelen</h2>\r\n<p>Het uitvoeren van een risicoanalyse in RAVIB resulteert in een lijst van geselecteerde maatregelen uit de gekozen standaard. In de één na laatste stap kan deze selectie aangepast worden door geselecteerde maatregelen geforceerd weg te laten of niet-geselecteerde maatregelen alsnog toe te voegen. Dit is een stap die het beste overgelaten kan worden aan iemand die goed thuis is in deze standaarden. Zorg ervoor dat deze persoon bij het maken van deze keuzes in overleg treedt met het management.</p>\r\n\r\n<h2>Verwerken van de uitkomsten</h2>\r\n<p>De laatste stap in het hele proces is het opstellen van een plan van aanpak op basis van de rapportage die door RAVIB gegenereerd kan worden. Dit is een kwestie van het kritisch doornemen van alle geselecteerde maatregelen. Indien de \'Gewenste situatie / te nemen acties\' en de \'Huidige situatie / huidige maatregelen\' zorgvuldig zijn ingevuld, geven deze een goed beeld van de te nemen stappen.</p>\r\n\r\n<p>Omdat een systeemeigenaar te allen tijde verantwoordelijk is voor de beveiliging van de systemen waar hij of zij eigenaar van is, dienen de te nemen stappen ten aanzien van een informatiesysteem met de eigenaar doorgesproken te worden. De systeemeigenaar is verantwoordelijk voor het laten uitvoeren van eventuele wijzigingen. Als beveiligingsadviseur of information security officer heb je niet meer dan een adviserende en controlerende rol. Maak met de eigenaar afspraken over wat er gewijzigd wordt en wanneer dit gebeurt en zie daar op toe. Wijzigingen die een systeemeigenaar niet wenst door te voeren, maar die vanuit de risicoanalyse wel als belangrijk of noodzakelijke worden gezien, dienen aan de directie voor een eindbeslissing te worden voorgelegd.</p>',1,0),(3,'/dpia','nl',NULL,0,NULL,'Data Protection Impact Assessment','Data Protection Impact Assessment','DPIA','<p>De data protection impact assessment (DPIA) module is te vinden op de <a href=\"https://www.privacy-friendly.nl/\">Privacy Friendly</a> website.</p>\r\n\r\n<p>Voorheen bevatte RAVIB een DPIA module. Omdat privacy een onderwerp is dat apart aandacht verdient, is daar een aparte website voor opgezet. Een offline (PDF) en een online versie van de DPIA module is daar te vinden. Op de weblog en opinie-pagina van deze website worden met enige regelmaat artikelen geplaatst over privacy en de AVG.</p>',1,0);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `progress_people`
--

DROP TABLE IF EXISTS `progress_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `progress_people` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`case_id`),
  CONSTRAINT `progress_people_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `progress_tasks`
--

DROP TABLE IF EXISTS `progress_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `progress_tasks` (
  `case_id` int(10) unsigned NOT NULL,
  `actor_id` int(10) unsigned DEFAULT NULL,
  `reviewer_id` int(10) unsigned DEFAULT NULL,
  `iso_measure_id` int(10) unsigned NOT NULL,
  `deadline` date DEFAULT NULL,
  `info` text NOT NULL,
  `done` tinyint(1) NOT NULL,
  `hours_planned` smallint(5) unsigned NOT NULL,
  `hours_invested` smallint(5) unsigned NOT NULL,
  KEY `case_id` (`case_id`),
  KEY `progress_people_id` (`actor_id`),
  KEY `iso_measure_id` (`iso_measure_id`),
  KEY `reviewer_id` (`reviewer_id`),
  CONSTRAINT `progress_tasks_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`),
  CONSTRAINT `progress_tasks_ibfk_3` FOREIGN KEY (`iso_measure_id`) REFERENCES `iso_measures` (`id`),
  CONSTRAINT `progress_tasks_ibfk_4` FOREIGN KEY (`actor_id`) REFERENCES `progress_people` (`id`),
  CONSTRAINT `progress_tasks_ibfk_5` FOREIGN KEY (`reviewer_id`) REFERENCES `progress_people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `profile` tinyint(4) DEFAULT '0',
  `session` tinyint(4) DEFAULT '0',
  `bia` tinyint(4) DEFAULT '0',
  `iso` tinyint(4) DEFAULT '0',
  `casus` tinyint(4) DEFAULT '0',
  `dreigingen` tinyint(4) DEFAULT '0',
  `rapport` tinyint(4) DEFAULT '0',
  `vergelijk` tinyint(4) DEFAULT '0',
  `cms` tinyint(4) DEFAULT '0',
  `cms/access` tinyint(4) DEFAULT '0',
  `cms/action` tinyint(4) DEFAULT '0',
  `cms/file` tinyint(4) DEFAULT '0',
  `cms/menu` tinyint(4) DEFAULT '0',
  `cms/measures` tinyint(4) DEFAULT '0',
  `cms/page` tinyint(4) DEFAULT '0',
  `cms/role` tinyint(4) DEFAULT '0',
  `cms/standards` tinyint(4) DEFAULT '0',
  `cms/settings` tinyint(4) DEFAULT '0',
  `cms/threats` tinyint(4) DEFAULT '0',
  `cms/user` tinyint(4) DEFAULT '0',
  `cms/validate` tinyint(4) DEFAULT '0',
  `risicomatrix` tinyint(4) DEFAULT '0',
  `koppelingen` tinyint(4) DEFAULT '0',
  `voortgang` tinyint(4) DEFAULT '0',
  `voortgang/personen` tinyint(4) DEFAULT '0',
  `voortgang/rapport` tinyint(4) DEFAULT '0',
  `cms/measures/categories` tinyint(4) DEFAULT '0',
  `cms/threats/categories` tinyint(4) DEFAULT '0',
  `voortgang/export` tinyint(4) DEFAULT '0',
  `actoren` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--
-- ORDER BY:  `id`

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrator',1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),(2,'User',1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,1,1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` varchar(128) NOT NULL,
  `content` text,
  `expire` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(50) NOT NULL,
  `name` tinytext,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) NOT NULL,
  `type` varchar(8) NOT NULL,
  `value` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--
-- ORDER BY:  `id`

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'admin_page_size','integer','20'),(2,'database_version','integer','5'),(3,'default_iso_standard','integer','2'),(4,'default_language','string','nl'),(5,'head_description','string','Gratis tool voor het uitvoeren van een risicoanalyse voor informatiebeveiliging op basis van de ISO 27002 of NEN 7510 standaard.'),(6,'head_keywords','string','risicoanalyse, informatiebeveiliging, dreigingen, ISO 27002, ISO 27001, NEN 7510, maatregelen, business impact analyse'),(7,'head_title','string','Risicoanalyse voor informatiebeveiliging'),(8,'hiawatha_cache_default_time','integer','3600'),(9,'hiawatha_cache_enabled','boolean','false'),(10,'page_after_login','string','casus'),(12,'secret_website_code','string','CHANGE_ME_INTO_A_RANDOM_STRING'),(13,'session_persistent','boolean','false'),(14,'session_timeout','integer','3600'),(15,'start_page','string','homepage'),(16,'webmaster_email','string','root@localhost');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `threat_categories`
--

DROP TABLE IF EXISTS `threat_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `threat_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `threat_categories`
--
-- ORDER BY:  `id`

LOCK TABLES `threat_categories` WRITE;
/*!40000 ALTER TABLE `threat_categories` DISABLE KEYS */;
INSERT INTO `threat_categories` VALUES (1,'Verantwoordelijkheid'),(2,'Wet- en regelgeving'),(3,'Continuïteit en betrouwbaarheid van systemen'),(4,'Incidentafhandeling'),(5,'Toegang tot informatie'),(6,'Uitwisselen en bewaren van informatie'),(8,'Menselijk handelen'),(9,'Fysieke beveiliging'),(10,'Bedrijfscontinuïteit');
/*!40000 ALTER TABLE `threat_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `threats`
--

DROP TABLE IF EXISTS `threats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `threats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(10) unsigned NOT NULL,
  `threat` tinytext NOT NULL,
  `description` text NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `confidentiality` enum('p','s','-') NOT NULL,
  `integrity` enum('p','s','-') NOT NULL,
  `availability` enum('p','s','-') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `threats_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `threat_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `threats`
--
-- ORDER BY:  `id`

LOCK TABLES `threats` WRITE;
/*!40000 ALTER TABLE `threats` DISABLE KEYS */;
INSERT INTO `threats` VALUES (1,1,'Gebrek aan sturing op informatiebeveiliging vanuit de directie.','De directie stuurt niet op informatiebeveiliging. Verantwoordelijkheden richting lijnmanagers zijn niet belegd. Een informatiebeveiligingsbeleid en/of ISMS ontbreekt.',1,'p','p','p'),(2,2,'Lijnmanagers nemen hun verantwoordelijkheid voor informatiebeveiliging niet.','Lijnmanagers zorgen er onvoldoende voor dat informatiebeveiliging binnen hun afdeling op de juiste manier wordt ingevuld. Het eigenaarschap van informatiesystemen is niet goed belegd. Beveiliging vormt geen vast onderdeel van projecten.',1,'p','p','p'),(3,4,'Medewerkers handelen onvoldoende naar hetgeen van hen verwacht wordt.','Het ontbreekt de medewerkers aan bewustzijn op het gebied van informatiebeveiliging en voelen onvoldoende noodzaak daaraan bij te dragen.',1,'p','p','p'),(7,36,'Wetgeving over informatie in de cloud.','Door wetgeving in sommige landen kan de overheid van zo\'n land inzage krijgen in informatie welke in de cloud ligt opgeslagen.',2,'p','',''),(8,37,'Buitenlandse wetgeving bij het bezoeken van een land.','Door wetgeving in sommige landen kan de overheid inzage eisen in de gegevens op meegenomen systemen bij een bezoek aan dat land.',2,'p','',''),(9,38,'Wetgeving over het gebruik van cryptografie.','Door wetgeving in sommige landen kan de overheid een kopie van cryptografische sleutels opeisen.',2,'p','',''),(11,6,'Toegang tot informatie wordt geblokkeerd.','Informatie op een systeem is ontoegankelijk gemaakt, doordat malware (ransomware, wipers) of een aanvaller deze informatie heeft versleuteld of verwijderd.',3,'','','p'),(12,7,'Netwerkdiensten raken overbelast.','Een netwerkdienst is verminderd beschikbaar door een moedwillige aanval (DoS) of door onvoorziene toename van de hoeveelheid verzoeken of van de benodigde resources om een verzoek af te handelen. Verzoeken kunnen komen vanuit gebruikers, maar ook vanuit andere systemen.',3,'','','p'),(13,39,'Incidenten worden niet tijdig opgepakt.','De gevolgen van incidenten worden hierdoor onnodig groot. Binnen het bedrijf is er onvoldoende netwerkmonitoring en is er geen centraal meldpunt voor beveiligingsincidenten.',4,'p','s',''),(14,40,'Informatie voor het aanpakken van incidenten ontbreekt.','Systeembeheerders hebben onvoldoende technische informatie over het probleem om het te kunnen oplossen. Een actieplan ontbreekt waardoor het incident onnodig lang blijft duren.',4,'p','p','p'),(15,41,'Herhaling van incidenten.','Veroorzakers van incidenten worden niet aangesproken op hun handelen. Managers hebben onvoldoende zicht op herhalende incidenten, waardoor zij daar niet op sturen.',4,'p','s','p'),(16,14,'Systemen worden niet gebruikt waarvoor ze bedoeld zijn.','Het ontbreken van een beleid op bijvoorbeeld het internetgebruik, vergroot de kans op misbruik.',8,'s','','p'),(17,15,'Wegnemen van bedrijfsmiddelen.','Door onvoldoende controle op de uitgifte en onjuiste inventarisatie van bedrijfsmiddelen bestaat de kans dat diefstal niet of te laat wordt opgemerkt.',8,'s','','p'),(18,16,'Beleid wordt niet gevolgd door ontbreken van sancties.','Door het ontbreken van sancties op het overtreden van regels bestaat de kans dat medewerkers de beleidsmaatregelen niet serieus nemen.',8,'p','s',''),(19,17,'Toelaten van externen in het pand of op het netwerk.','Het toelaten van externen, zoals leveranciers en projectpartners, kunnen gevolgen hebben voor de vertrouwelijkheid van de informatie die binnen het pand of via het netwerk beschikbaar is.',8,'p','',''),(20,19,'Misbruik van andermans identiteit.','Door onvoldoende (mogelijkheid op) controle op een identiteit, kan ongeautoriseerde toegang verkregen worden tot vertrouwelijke informatie. Denk hierbij ook aan social engineering, zoals phishing en CEO-fraude.',8,'p','p',''),(21,21,'Onterecht hebben van rechten.','Door een ontbrekend, onjuist of onduidelijk proces voor het uitdelen en innemen van rechten, kan een persoon onbedoeld meer rechten hebben. Deze rechten kunnen door deze persoon zelf of door anderen (bijv. via malware) misbruikt worden.',8,'p','p',''),(22,20,'Misbruik van speciale bevoegdheden.','Door onvoldoende controle op medewerkers met bijzondere rechten, zoals systeembeheerders, bestaat de kans op ongeautoriseerde toegang tot gevoelige informatie.',8,'p','p',''),(23,22,'Slecht wachtwoordgebruik.','Het ontbreken van een wachtwoordbeleid en bewustzijn bij medewerkers kan leiden tot het gebruik van zwakke wachtwoorden, het opschrijven van wachtwoorden of het gebruik van hetzelfde wachtwoord voor meerdere systemen.',5,'p','p',''),(24,23,'Onbeheerd achterlaten van werkplekken.','Door het ontbreken van een clear-desk en/of clear-screen policy kan toegang verkregen worden tot gevoelige informatie.',5,'p','s',''),(25,24,'Onduidelijkheid over classificatie en bevoegdheden.','Door onduidelijkheid over vertrouwelijkheid van informatie van informatie en bevoegdheid van personen bestaat de kans op ongeautoriseerde toegang tot gevoelige informatie.',5,'p','s',''),(26,25,'Informatie op systemen bij reparatie of verwijdering.','Gevoelige informatie kan lekken indien opslagmedia of systemen welke opslagmedia bevatten worden weggegooid of ter reparatie aan derden worden aangeboden.',5,'p','',''),(27,35,'Misbruik van cryptografische sleutels en/of gebruik van zwakke algoritmen.','Door een onjuist of ontbrekend sleutelbeheer bestaat de kans op misbruik van cryptografische sleutels. Het gebruik van zwakke cryptografische algoritmen biedt schijnveiligheid.',6,'p','p',''),(28,26,'Misbruik van kwetsbaarheden in applicaties of hardware.','Kwetsbaarheden in applicaties of hardware worden misbruikt (exploits) om ongeautoriseerde toegang te krijgen tot een applicatie en de daarin opgeslagen informatie.',5,'p','p',''),(29,27,'Misbruiken van zwakheden in netwerkbeveiliging.','Zwakheden in de beveiliging van het (draadloze) netwerk worden misbruikt om toegang te krijgen tot dit netwerk.',5,'p','p',''),(30,29,'Informatie buiten de beschermde omgeving.','Informatie die voor toegestaan gebruik meegenomen wordt naar bijvoorbeeld buiten het kantoor wordt niet meer op de juiste wijze beschermd. Denkbij ook aan Bring Your Own Device (BYOD).',5,'p','',''),(31,30,'Afluisterapparatuur.','Door middel van keyloggers of netwerktaps wordt gevoelige informatie achterhaald.',5,'p','',''),(32,31,'Onveilig versturen van gevoelige informatie.','Inbreuk op vertrouwelijkheid van informatie door onversleuteld versturen van informatie.',6,'p','s',''),(33,32,'Versturen van gevoelige informatie naar onjuiste persoon.','Inbreuk op vertrouwelijkheid van informatie door het onvoldoende controleren van ontvanger.',6,'p','',''),(35,33,'Informatieverlies door verlopen van houdbaarheid van opslagwijze.','Informatie gaat verloren door onleesbaar geraken van medium of gedateerd raken van bestandsformaat.',6,'','','p'),(36,34,'Foutieve informatie.','Ongewenste handelingen als gevolg van foutieve bedrijfsinformatie of het toegestuurd krijgen van foutieve informatie. Dit kan zijn als gevolg van moedwillig handelen of van een vergissing.',6,'','p',''),(37,18,'Verlies van mobiele apparatuur en opslagmedia.','Door het verlies van mobiele apparatuur en opslagmedia bestaat de kans op inbreuk op de vertrouwelijkheid van gevoelige informatie.',8,'p','','s'),(38,8,'Aanvallen via systemen die niet in eigen beheer zijn.','Door onvoldoende grip op de beveiliging van prive- en thuisapparatuur en andere apparatuur van derden, bestaat de kans op bijvoorbeeld besmetting met malware.',3,'','','p'),(39,11,'Uitval van systemen door softwarefouten.','Fouten in software kunnen leiden tot systeemcrashes of het corrupt raken van de in het systeem opgeslagen informatie.',3,'','p','p'),(40,10,'Uitval van systemen door configuratiefouten.','Onjuiste configuratie van een applicatie kan leiden tot een verkeerde verwerking van informatie.',3,'','p','s'),(41,9,'Uitval van systemen door hardwarefouten.','Hardware van onvoldoende kwaliteit kan leiden tot uitval van systemen.',3,'','','p'),(42,13,'Gebruikersfouten.','Onvoldoende kennis of te weinig controle op andermans werk vergroot de kans op menselijke fouten. Gebruikersinterfaces die niet zijn afgestemd op het gebruikersniveau verhogen de kans op fouten.',8,'','p','s'),(43,50,'Software wordt niet meer ondersteund door de uitgever.','Voor software die niet meer ondersteund wordt worden geen securitypatches meer uitgegeven. Denk ook aan Excel- en Access-applicaties.',10,'','','p'),(44,42,'Ongeautoriseerde fysieke toegang.','Het ontbreken van toegangspasjes, zicht op ingangen en bewustzijn bij medewerkers vergroot de kans op ongeautoriseerde fysieke toegang.',9,'p','',''),(45,43,'Brand.','Het ontbreken van brandmelders en brandblusapparatuur vergroten de gevolgen van een brand.',9,'','','p'),(46,45,'Overstroming en wateroverlast.','Overstroming en wateroverlast kunnen zorgen voor schade aan computers en andere bedrijfsmiddelen.',9,'','','p'),(47,46,'Verontreiniging van de omgeving.','Verontreininging van de omgeving kan ertoe leiden dat de organisatie (tijdelijk) niet meer kan werken.',9,'','','p'),(48,44,'Explosie.','Explosies kunnen leiden tot schade aan het gebouw en apparatuur en slachtoffers.',9,'','','p'),(49,47,'Uitval van facilitaire middelen (gas, water, electra, airco).','Uitval van facilitaire middelen kan tot gevolg hebben dat een of meerdere bedrijfsonderdelen hun werk niet meer kunnen doen.',9,'','','p'),(50,48,'Vandalisme','Schade aan of vernieling van bedrijfseigendommen als gevolg van een ongerichte actie, zoals vandalisme of knaagdieren.',9,'','','p'),(52,49,'Niet beschikbaar zijn van diensten van derden.','Het niet meer beschikbaar zijn van diensten van derden door uitval van systemen, faillissement, ongeplande contractbeëindiging of onacceptabele wijziging in de dienstverlening (bijvoorbeeld door bedrijfsovername).',10,'','','p'),(53,51,'Kwijtraken van belangrijke kennis bij niet beschikbaar zijn van medewerkers.','Medewerkers die het bedrijf verlaten of door een ongeval voor lange tijd niet inzetbaar zijn, bezitten kennis die daardoor niet meer beschikbaar is.',10,'','','p'),(56,12,'Fouten als gevolg van wijzigingen in andere systemen.','In een systeem ontstaan fouten als gevolg van wijzigingen in gekoppelde systemen.',3,'','p','p'),(57,5,'Onvoldoende aandacht voor beveiliging bij softwareontwikkeling.','Onvoldoende aandacht voor beveiliging bij het zelf of laten ontwikkelen van software leidt tot inbreuk op de informatiebeveiliging.',3,'p','p','p'),(58,28,'Onvoldoende aandacht voor beveiliging bij uitbesteding van werkzaamheden.','Doordat externe partijen / leveranciers hun informatiebeveiliging niet op orde hebben, kunnen inbreuken ontstaan op de informatie waar zij toegang tot hebben.',5,'p','',''),(60,3,'Onvoldoende aandacht voor beveiliging binnen projecten.','Binnen projecten (exclusief softwareontwikkeling) is onvoldoende aandacht voor beveiliging. Dit heeft negatieve gevolgen voor nieuwe systemen en processen die binnen de organisatie worden geïntroduceerd.',1,'p','s','p');
/*!40000 ALTER TABLE `threats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  KEY `role_id` (`role_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `password` varchar(128) NOT NULL,
  `one_time_key` varchar(128) DEFAULT NULL,
  `status` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `authenticator_secret` varchar(16) DEFAULT NULL,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `organisation_id` (`organisation_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--
-- ORDER BY:  `id`

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'admin','none',NULL,1,NULL,'Beheerder','root@localhost','2019-01-01 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-20 21:09:10
