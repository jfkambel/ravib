<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />
<xsl:include href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover table-xs">
<thead>
<tr><th>Actor</th><th>Kans / bereidheid</th><th>Kennisniveau</th><th>Middelen</th></tr>
</thead>
<tbody>
<xsl:for-each select="actors/actor">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{../../../case/@id}/{@id}'">
<td><span class="header">Actor:</span><xsl:value-of select="name" /></td>
<td><span class="header">Kans / bereidheid:</span><xsl:value-of select="chance" /></td>
<td><span class="header">Kennisniveau:</span><xsl:value-of select="knowledge" /></td>
<td><span class="header">Middelen:</span><xsl:value-of select="resources" /></td>
</tr>
</xsl:for-each>
<tr>
<td colspan="4"><a href="/{/output/page}/{../case/@id}/new" class="btn btn-xs btn-primary">+</a></td>
</tr>
</tbody>
</table>

<div class="right">
<xsl:apply-templates select="pagination" />
</div>

<div class="btn-group left">
<a href="/{/output/page}/{../case/@id}/export" class="btn btn-default">Exporteer BIA-en-actorenoverzicht</a>
</div>
<div class="btn-group right">
<a href="/dreigingen/{../case/@id}" class="btn btn-default">Verder naar dreigingen</a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}/{../case/@id}" method="post">
<xsl:if test="actor/@id">
<input type="hidden" name="id" value="{actor/@id}" />
</xsl:if>

<label for="name">Naam actor:</label>
<input type="text" id="name" name="name" value="{actor/name}" class="form-control" />
<label for="chance">Kans op veroorzaken incident of bereidheid tot aanval:</label>
<select name="chance" class="form-control">
<xsl:for-each select="chance/item"><option value="{position()}"><xsl:if test="position()=../../actor/chance"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
<label for="knowledge">Kennisniveau:</label>
<select name="knowledge" class="form-control">
<xsl:for-each select="knowledge/item"><option value="{position()}"><xsl:if test="position()=../../actor/knowledge"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
<label for="resources">Middelen:</label>
<select name="resources" class="form-control">
<xsl:for-each select="resources/item"><option value="{position()}"><xsl:if test="position()=../../actor/resources"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
<label for="reason">Doel van aanval of oorzaak van incident:</label>
<input type="text" id="reason" name="reason" value="{actor/reason}" class="form-control" />

<div class="btn-group">
<input type="submit" name="submit_button" value="Actor opslaan" class="btn btn-default" />
<a href="/{/output/page}/{../case/@id}" class="btn btn-default">Afbreken</a>
<xsl:if test="actor/@id">
<input type="submit" name="submit_button" value="Actor verwijderen" class="btn btn-default" onClick="javascript:return confirm('VERWIJDEREN: Weet je het zeker?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Actoren</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
<div id="help">
<p>In dit onderdeel geeft u de actoren aan die een concreet beveiligingsincident kunnen veroorzaken. Dit kan als gevolg van een onbedoelde actie zijn, waarbij u moet kijken naar de kans dat de actor een incident veroorzaakt. Het kan ook een bedoelde actie zijn, waarbij u moet kijken naar de bereidheid van een actor om u daadwerkelijk aan te vallen. Het kennisniveau geeft de kennis van de actor op het gebied van cybersecurity en digitale aanvallen aan.</p>
<p>Het is toegestaan om een actor meerdere malen te noemen, waarbij verschillende waarden voor kans/bereidheid en kennisniveau worden gekozen. Bijvoorbeeld:</p>
<table class="table table-condensed table-striped">
<thead>
<tr><th>Actor</th><th>Kans / bereidheid</th><th>Kennisniveau</th><th>Middelen</th></tr>
</thead>
<tbody>
<tr><td>Medewerkers</td><td>gering</td><td>basis</td><td>n.v.t.</td></tr>
<tr><td>Medewerkers (ICT-er)</td><td>gering</td><td>kundig</td><td>n.v.t.</td></tr>
<tr><td>Medewerkers (ontevreden)</td><td>mogelijk</td><td>basis</td><td>beperkt</td></tr>
</tbody>
</table>
</div>
</xsl:template>

</xsl:stylesheet>
