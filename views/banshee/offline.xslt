<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../banshee/main.xslt" />

<xsl:template match="content">
<h1>Website offline</h1>
<p>Sorry, de RAVIB website is tijdelijk niet beschikbaar vanwege onderhoudswerkzaamheden.</p>
</xsl:template>

</xsl:stylesheet>
